/// <reference types="Cypress" />

// The test automation should cover the following scenarios:
// 1. Login to the application
// 2. Validate landing page
// 3. Validate add and remove a task.
// 4. Validate marking/unmarking a task as done.
// 5. Validate all tasks page.
// 6. Validate favorites page.

describe('Task Manager Application test suit', function () {
    before(function () {
        cy.fixture('userLoginData').then(function (data) {
            this.data = data
        })
    })
    // 1. Login to the application
    it('User can Login to Task Manager Application ', function () {
        cy.visit('/')
        // cy.visit("http://localhost:4200/");
        cy.title().should('include', 'Task Manager');

        // this.data.loginEmail
        this.data.loginEmail.forEach(function (element) {
            cy.get('#mat-input-0').type(element)
            cy.get('#mat-input-1').type("user")
            cy.get('.mat-card-actions > .login-field').click()
        });
    })
    // 2. Validate landing page
    it('after successful Login we land at task manager my day screen', () => {
        cy.title().should('include', 'Task Manager')
        cy.get('.mat-elevation-z6 > [fxflex="100%"]').should('contain', 'My day')
        cy.get(':nth-child(1) > .mat-list-item > .mat-list-item-content > [fxflex="100%"]').should('contain', 'My day')
    })

    // 3. Validate add and remove a task.
    it('task manager my day screen contains add and remove task', () => {
        cy.get('#addTask > .mat-button-wrapper').should('contain', 'Add')
        cy.get('.remove-icon').should('have.class', 'remove-icon mat-icon notranslate material-icons mat-icon-no-color')
    })

    // 4. Validate marking/unmarking a task as done.
    // BUG001 -----> when check box is clicked, no tick appears within the checkbox, but task is marked as completed with Strikethru over text.
    it('task manager my day screen Validate marking and unmarking a task as done', () => {
        cy.get('#mat-checkbox-2 > .mat-checkbox-layout > .mat-checkbox-inner-container').click
        // cy.get('.completed-task')
        
    })
    // 5. Validate all tasks page.
    it('task manager All Task screen validation', () => {
        cy.get(':nth-child(2) > .mat-list-item > .mat-list-item-content > [fxflex="100%"]').click()
        cy.get('.mat-elevation-z6 > [fxflex="100%"]').should('contain', 'All Tasks')
    })

    // 6. Validate favorites page.
    it('task manager Favourites screen validation', () => {
        cy.get(':nth-child(3) > .mat-list-item > .mat-list-item-content > [fxflex="100%"]').click()
        cy.get('.mat-elevation-z6 > [fxflex="100%"]').should('contain', 'Important Tasks')
    })






}
)